import json
import boto3
from datetime import datetime
from datetime import timedelta
from datetime import timezone
from boto3.dynamodb.conditions import Key, Attr

        

def lambda_handler(event, context):
    # TODO implement
    print(event)

    response = []
    TaskList = []

    DueDate = datetime.now() - timedelta(hours=-12)
    remainderDate = datetime.now() - timedelta(hours=12)
    print('DueDate ', DueDate)
    print('remainderDate ', remainderDate)
    
    tblName = boto3.resource('dynamodb').Table('SMSMappingTable')
    
    if event['status'] == 'Send':
        done = False
        start_key = None
        while not done:
            if start_key is None :
                response = tblName.scan(FilterExpression=Attr('SMSSendStatus').eq('smsDelivered'))
            else:
                response = tblName.scan(ExclusiveStartKey=start_key, FilterExpression=Attr('SMSSendStatus').eq('smsDelivered'))
            
            print('response',response)
            print('ScannedCount',response['ScannedCount'])
            print('Count',response['Count'])
            start_key = response.get('LastEvaluatedKey', None)
            done = start_key is None 
        
        for item in response['Items']:
            TaskData = {
                'Id' : item['Id'],
                'ToName': item['ToName'],
                'WhatId' : item['LeadId'],
                'Status': 'Completed',
                'Description' : item['Body'],
                'FileName': item['FileName'],
                ##'ActivityDate' : DueDate.isoformat(),
                'Subject': 'Send SMS to ' + str(item['ToName']),
                ##'IsReminderSet': 'true',
                ##'ReminderDateTime': remainderDate.isoformat()
            }
            TaskList.append(TaskData)
        

    if event['status'] == 'Receive':
          
        done = False
        start_key = None
        while not done:
            if start_key is None :
                response = tblName.scan(FilterExpression=Attr('SMSRecvStatus').eq('smsReceived'))
            else:
                response = tblName.scan(ExclusiveStartKey=start_key, FilterExpression=Attr('SMSRecvStatus').eq('smsReceived'))
            
            print('ScannedCount for smsreceive',response['ScannedCount'])
            print('Count for smsreceive',response['Count'])
            start_key = response.get('LastEvaluatedKey', None)
            done = start_key is None 
            
        for item in response['Items']:
            TaskData = {
                'Id' : item['Id'],
                'ToName': item['ToName'],
                'WhatId' : item['LeadId'],
                'Status': 'Open',
                'Description' : item['SMSRecvBody'],
                ##'ActivityDate' : DueDate.isoformat(),
                'Subject': 'Incoming SMS',
                'FileName': item['FileName'],
                ##'IsReminderSet': 'true',
                ##'ReminderDateTime': remainderDate.isoformat()
            }
            print(TaskData)
            TaskList.append(TaskData)
    print('taskdata',json.dumps(TaskList))    
    return {
        'statusCode': 200,
        'body': json.dumps(TaskList)
    }

