import json
import botocore
import boto3, os, csv
from io import BytesIO, StringIO
import pandas as pd

def convert_csv_to_json_list(buffer):
    items = []
    reader = csv.DictReader(buffer)
    for row in reader:
        print('row', row)
        items.append(row)
    return items


def create_table_dynadb(tblName='CountryCodesTest'):

    try:
        response = boto3.resource('dynamodb', region_name='us-east-1').create_table(
            AttributeDefinitions=[
                {
                    'AttributeName': 'country',
                    'AttributeType': 'S',  # 'S'|'N'|'B'
                },
            ],
            KeySchema=[
                {
                    'AttributeName': 'country',
                    'KeyType': 'HASH',
                },
            ],
            BillingMode='PAY_PER_REQUEST',
            TableName=tblName,
        )
        print(response)
    except botocore.exceptions.ClientError as error:
        # Put your error handling logic here
        print('ClientError ', error)

    except botocore.exceptions.ParamValidationError as error:
        print('ParamValidationError ', ValueError('The parameters you provided are incorrect: {}'.format(error)))
        
def lambda_handler(event, context):



    create_table_dynadb()
    tblName = boto3.resource('dynamodb', region_name='us-east-1').Table('CountryCodesTest')

    res = boto3.client('s3').get_object(Bucket=os.getenv('BucketName', 'tino-country-codes'), Key='tino_country_codes.csv')
    buffer = BytesIO(res["Body"].read())
    
    
    df = pd.read_csv(buffer)
    print(df.shape, df.columns)
    json_result = df.to_json(orient="records")
    request_items = json.loads(json_result)    
    ##json_data = convert_csv_to_json_list('file')

    with tblName.batch_writer() as batch:
        # Loop through the JSON objects
        for item in request_items:
            batch.put_item(Item=item)
