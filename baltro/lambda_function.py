import json
import boto3, os, botocore
import requests
import time


def cfn_send(event, context, responseStatus, responseData):
    responseUrl = event['ResponseURL']

    print(responseUrl)

    responseBody = {
        'Status' : responseStatus,
        'Reason' : "See the details in CloudWatch Log Stream: {}".format(context.log_stream_name),
        'PhysicalResourceId' : context.log_stream_name,
        'StackId' : event['StackId'],
        'RequestId' : event['RequestId'],
        'LogicalResourceId' : event['LogicalResourceId'],
        'NoEcho' : 'noEcho',
        'Data' : responseData
    }

    json_responseBody = json.dumps(responseBody)

    print("Response body:")
    print(json_responseBody)

    headers = {
        'content-type' : '',
        'content-length' : str(len(json_responseBody))
    }

    try:
        response = requests.put(responseUrl, headers=headers, data=json_responseBody)
        print("Status code:", response)


    except Exception as e:

        print("send(..) failed executing http.request(..):", e)
        

def lambda_handler(event, context):
    # TODO implement

    ReqSess = requests.Session()

    fullName = os.getenv('fullName','anba123')
    email = os.getenv('email', 'harshitha1018@test.com')
    tenantName = os.getenv('tenantName','anba123')
    apiUrl = os.getenv('apiUrl','anba123')
    AccessKey = os.getenv('AccessKey','anba123')
    SecretAccessKey = os.getenv('SecretKey','anba123')
    databucket = os.getenv('databucket', 'ali1-test,ali2-test')
    outputbucket = os.getenv('outputbucket', '')
    
    if len(outputbucket.strip()) == 0:
        outputbucket = databucket
    

    

    BucketList = databucket.split(',')
    bucketInfo = []
    S3Resources = []
    
    region = os.getenv('AWS_REGION')
    
    for bl in BucketList:
        try:
            res = boto3.client('s3').get_bucket_location(Bucket=bl)
            print('Bucket Location ', res.keys())
            S3Resources.append('arn:aws:s3:::' + bl)
            
            for k in res.keys():
                print(bl, ' ', k , ' ', res[k])
                if 'LocationConstraint' in k:
                    if res[k] != None:
                        region = res[k]
                    else:
                        region = 'us-east-1'
                        
                
            binfo = {}
            binfo['dataBucketName'] = bl
            binfo['accessId'] = AccessKey
            binfo['secretKey'] = SecretAccessKey
            binfo['outBucketName'] = bl
            binfo['region'] = region
            
            bucketInfo.append(binfo)
        except ValueError:
            if 'ResponseURL' in event.keys():
                return cfn_send(event, context, 'FAILED', res) 
            else:
                print(ValueError)
                return
            
    IAMPolicy = {
                    "Version": "2012-10-17",
                    "Statement": [
                        {
                            "Effect": "Allow",
                            "Action": [
                                "s3:*"
                            ],
                            "Resource": S3Resources
                        }
                    ]
                }
    IAMPolicy = json.dumps(IAMPolicy)           
    print(bucketInfo)
    print(IAMPolicy)

    PolicyName='BaltroS3DataBucketAccess'
    PolicyArn = 'arn:aws:iam::' + context.invoked_function_arn.split(":")[4] + ':policy/' + PolicyName
    print(PolicyArn)
    
    try:
        response = boto3.client('iam').detach_user_policy(
            UserName=os.getenv('IAMUserName', 'harshibaltrotest'),
            PolicyArn=PolicyArn
        )
    except:
        pass
    

    try:
        response = boto3.client('iam').delete_policy(
            PolicyArn=PolicyArn
        )
    except:
        pass    

    
    response = boto3.client('iam').create_policy(
        PolicyName=PolicyName,
        PolicyDocument=IAMPolicy,
        Description='Baltro S3 Data Bucket Access for IAM User'
    )

    response = boto3.client('iam').attach_user_policy(
        UserName=os.getenv('IAMUserName', 'harshibaltrotest'),
        PolicyArn=PolicyArn
    )
    
    time.sleep(15)
    data = {
    "email": email,
    "fullName": fullName,
    "tenantName": tenantName,
    "region" : region
    }
    
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    try:
        r = ReqSess.post(apiUrl, data=json.dumps(data), headers=headers)
    except ValueError:
        if 'ResponseURL' in event.keys():
            return cfn_send(event, context, 'FAILED', r) 
        else:
            print(ValueError)
            return
        
    Data = r.json()
    if Data['hasError'] == 'true':
        if 'ResponseURL' in event.keys():
            return cfn_send(event, context, 'FAILED', Data)
        else:
            print('Error ', Data)
            return
            
    if 'data' in Data.keys():
        ClientData = json.loads(Data['data'])
        print(ClientData)
        if 'tid' in ClientData.keys() and 'hid' in ClientData.keys() and 'apikey' in ClientData.keys():
            tid = ClientData['tid']
            hid = ClientData['hid']
            api_key = ClientData['apikey']
            url = ClientData['url']
            print(url,tid,hid,api_key)
            if len(api_key) > 0:
                boto3.client('s3').put_object(
                            Bucket=BucketList[0],
                            Key='BaltoroDataFile.txt',
                            Body=api_key,
                            ACL='private'
                            )
            
            BucketData = {
                "bucketInfo": bucketInfo
            }
    
            headers = {'apikey': api_key, 'email' : email, 'hid': hid, 'tid': tid }

            try:
                api2_res = ReqSess.get(url)
            except ValueError:
                if 'ResponseURL' in event.keys():
                    return cfn_send(event, context, 'FAILED', api2_res) 
                else:
                    print(ValueError)
                    return
            api2_Data = api2_res.json()
            print('second api response',api2_Data)

            if api2_Data['hasError'] == 'true':
                if 'ResponseURL' in event.keys():
                    return cfn_send(event, context, 'FAILED', api2_Data)
                else:
                    print('Error ', api2_Data)
                    return

            if 'data' in api2_Data.keys():
                ClientData = json.loads(api2_Data['data'])
                sec_api_res_url = ClientData['url']
                try:
                    api3_Res = ReqSess.get(sec_api_res_url, data=json.dumps(BucketData), headers=headers)
                except ValueError:
                    if 'ResponseURL' in event.keys():
                        return cfn_send(event, context, 'FAILED', api3_Res) 
                    else:
                        print(ValueError)
                        return
        
                api3_Data = api3_Res.json()
                print('third_api_response',api3_Data)
                if api3_Data['hasError'] == 'true':
                    if 'ResponseURL' in event.keys():
                        return cfn_send(event, context, 'FAILED', api3_Data)
                    else:
                        print('Error ', api3_Data)
                        return
                else:
                    if 'ResponseURL' in event.keys():
                        return cfn_send(event, context, 'SUCCESS', api3_Data)
        





