import json
import botocore, boto3

def lambda_handler(event, context):
    # TODO implement
    ##event = {'Id': '00Q5f00000AGFYXEA50025f000000ri1AAAQ', 'Status': 'task created'}
    print('recv update event data ',event)
    table = boto3.resource('dynamodb').Table('SMSMappingTable')

    SMSStatus = ''
    
    if event['getStatus'] == 'Send':
        SMSStatus = 'SMSSendStatus'
        
    if event['getStatus'] == 'Receive':
        SMSStatus = 'SMSRecvStatus'   
        
    if len(SMSStatus) == 0:
        return 
    
    try:
        table.update_item(
            Key={
                'Id': event['Id'],
            },
            UpdateExpression= 'SET '+ SMSStatus + ' = :val1', 
            ExpressionAttributeValues={
                ':val1': event['Status'],
            }
        )
    except botocore.exceptions.ClientError as error:
        # Put your error handling logic here
        print('Put Item Error ', error)
    
    except botocore.exceptions.ParamValidationError as error:
        print('ParamValidationError ', ValueError('The parameters you provided are incorrect: {}'.format(error)))
                            
    
    
    return {
        'statusCode': 200,
        'body': json.dumps('Hello from Lambda!')
    }

