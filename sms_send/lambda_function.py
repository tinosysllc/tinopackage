import json
import boto3, re, botocore
import urllib
from urllib.parse import urlparse
import pandas as pd
from datetime import datetime
from datetime import timedelta, timezone
from io import BytesIO, StringIO
import os
import time


def create_table_dynadb(tblName='SMSMappingTable'):

    try:
        response = boto3.resource('dynamodb').create_table(
            AttributeDefinitions=[
                {
                    'AttributeName': 'Id',
                    'AttributeType': 'S',  # 'S'|'N'|'B'
                },
            ],
            KeySchema=[
                {
                    'AttributeName': 'Id',
                    'KeyType': 'HASH',
                },
            ],
            BillingMode='PAY_PER_REQUEST',
            TableName=tblName,
        )
        print(response)

    except botocore.exceptions.ClientError as error:
        # Put your error handling logic here
        print('ClientError ', error)

    except botocore.exceptions.ParamValidationError as error:
        print('ParamValidationError ', ValueError('The parameters you provided are incorrect: {}'.format(error)))
       

def WritetoS3CSV(df,filename):
    
    ##df = pd.read_csv(filename)
    

    print(df.keys())
    print(df.head())

    tblName='SMSMappingTable'
    create_table_dynadb(tblName)
    
    for i in range(len(df.index)):
        ##print(df.iloc[i]['Phone'])
        '''
        number = ''.join(df.iloc[i]['Phone'])
        number = '16462069413'
        print(number)
        meesage = 'Hello How are you';
        res = sns.publish(PhoneNumber=number,
                    Message=meesage
                    )
        Id	Phone	Country	Name	Owner.Name	Owner.Phone	LastModifiedBy.Name
        '''
        if 'sms' not in str(df.iloc[i]['docTitle']).lower():
            print('Subject deosn;t have SMS : ', str(df.iloc[i]['docTitle']))
            continue
        Body = ''
        if str(df.iloc[i]['docDescription']) == 'nan':
            ##Body  = df.iloc[i]['Owner.Name'] + ', The Barrett Group'
            Body  = ''
            continue
        else:
            Body = str(df.iloc[i]['docDescription']) + ' ' + df.iloc[i]['Owner.Name']
            #Body = str(df.iloc[i]['docDescription']) + ' ' + df.iloc[i]['Owner.Name'] + ', The Barrett Group'
            

        try:
            res = boto3.resource('dynamodb').Table(tblName).put_item(
                Item={
                     'Id': df.iloc[i]['Id'] + df.iloc[i]['docId'],
                     'LeadId': df.iloc[i]['Id'] ,
                     'DocId': df.iloc[i]['docId'],
                     'ToCountry' : df.iloc[i]['Country'],
                     'ToName':df.iloc[i]['Name'],
                     'ToNumber': str(df.iloc[i]['Phone']),
                     'ToMessage': df.iloc[i]['Name'],
                     'FromCountry' : df.iloc[i]['Country'],
                     'FromNumber': df.iloc[i]['Owner.Phone'],
                     'FromName': df.iloc[i]['LastModifiedBy.Name'],
                     'Subject' : str(df.iloc[i]['docTitle']),
                     'Body' : Body,
                     'FileName' : filename,
                     'SendMessageId' : '',

                }
            )
            print('res',res)
        except botocore.exceptions.ClientError as error:
            # Put your error handling logic here
            print('Put Item Error ', error)

        except botocore.exceptions.ParamValidationError as error:
            print('ParamValidationError ', ValueError('The parameters you provided are incorrect: {}'.format(error)))
           
def csv2json(csvFilePath, jsonFilePath):
    # Open a csv reader called DictReader
    with open(csvFilePath, encoding='utf-8') as csvf:
        csvReader = csv.DictReader(csvf)
         
        # Convert each row into a dictionary
        # and add it to data
        for rows in csvReader:
             
            # Assuming a column named 'No' to
            # be the primary key
            key = rows['No']
            data[key] = rows
 
    # Open a json writer, and use the json.dumps()
    # function to dump data
    with open(jsonFilePath, 'w', encoding='utf-8') as jsonf:
        jsonf.write(json.dumps(data, indent=4))
        
def lambda_handler(event, context):
    # TODO implement
    #print(event)
    #sfdc_update_crm()

    s3 = boto3.client('s3')
    sns = boto3.client('sns')
    

    
    if type(event) == dict:
        if 'Records' in event.keys():
            inputBucket = event['Records'][0]['s3']['bucket']['name']
            filePrefix = event['Records'][0]['s3']['object']['key']
            filePrefix = urllib.parse.unquote_plus(filePrefix)
            print(inputBucket,filePrefix)
            x = filePrefix.split("/", 1)
            filename = x[1]
        else:
            inputBucket = os.getenv('sms_bucket_name','sfdc-notes')
            filePrefix = 'sms/'
            pobjs = s3.list_objects_v2(Bucket=inputBucket, Prefix=filePrefix)

            if pobjs['KeyCount'] == 0:
                return

            lastModCsvFile = max(pobjs['Contents'], key=lambda x: x['LastModified'])
            
            print('LastModified ', lastModCsvFile['LastModified'])
            
            print('KeyCount = ', pobjs['KeyCount'])
            d = datetime.now(timezone.utc) - timedelta(hours=0, minutes=1)
            print('Date minus 1 min', d)
            files = [ obj['Key'] for obj in pobjs['Contents'] if obj['LastModified'] < d ]
            print('files found', files)
            for f in files:
                ##print('file Name with full path', f)
                filename = f.split('/', 1)[-1]
                ##print('filename', filename)
                boto3.client('s3').download_file(inputBucket, f, '/tmp/' + filename)
                ##os.system('cat ' + '/tmp/' + filename)
                df = pd.read_csv('/tmp/' + filename)
                print(df.columns)
                WritetoS3CSV(df, filename)
                ##csv2json('/tmp/' + filename, '/tmp/'+filename.split('.csv')[0]+'.json')
                #res = s3.get_object(Bucket=inputBucket, Key=f)
                #buffer = BytesIO(res["Body"].read())
                '''
                try:
                    df = pd.read_csv('/tmp/' + filename)
                    ##print(df.columns)
                except:
                    pass
                    print('Corrupted Files : ', f)
                '''

                time.sleep(3)
                s3.delete_object(Bucket=inputBucket, Key=f)

                ##boto3.client('s3').download_file(inputBucket, f, '/tmp/TinoTmp')
                ##print(os.listdir('/tmp/'))

                ##time.sleep(5)
                ##s3.delete_object(Bucket=inputBucket, Key=f)

            
    '''
            for Content in pobjs['Contents']:
                print(Content['Key'])
                FilesList.append(Content['Key'])
                    

    for k in FilesList:
        filename = filePrefix.split("/", 1)[1]
    '''
  
    return



