import json, boto3, os
from simple_salesforce import Salesforce
from datetime import datetime
from datetime import timedelta
from datetime import timezone
import botocore

import pytz
from boto3.dynamodb.conditions import Key, Attr

def create_taskfor_sms(sfdc_username,sfdc_pass,sfdc_token, LeadId, messageBody,Name):
    
    sf = Salesforce(username=sfdc_username, password=sfdc_pass, security_token=sfdc_token)

    #sf = Salesforce(username='anba@careerchange.com.tinochange', password='Welcome123$', security_token='88YnC7grtt6NYa3DJXFjGguP',instance_url= 'https://businesscoachingofnortherncalifornia.lightning.force.com/', domain='test')
    #sf = Salesforce(username='sneha@careerchange.com.tinotest', password='Welcome123$', security_token='EomVDr9NSPHEabJvbi75UNXt5',instance_url= 'https://businesscoachingofnortherncalifornia.lightning.force.com/', domain='test')
    print(Name + ' ' + LeadId)
    DueDate = datetime.now() - timedelta(hours=-12)
    remainderDate = datetime.now() - timedelta(hours=12)
    print('DueDate ', DueDate)
    print('remainderDate ', remainderDate)

    query = 'select Id from Contact where Name = \''+  Name.strip() + '\''
    print(query)
    query = sf.bulk.Contact.query(str(query))
    print('query results ', query)
    for row in query:
        print(row['Id'], ' ', LeadId)
        if row['Id'] != LeadId:
            data = {
                'WhoId': row['Id'], 
                'WhatId' : LeadId,
                'Status': 'Open',
                'Description' : messageBody,
                'ActivityDate' : DueDate.isoformat(),
                'Subject': 'Incoming SMS',
                'IsReminderSet': 'true',
                'ReminderDateTime': remainderDate.isoformat()
            }
            print(data)
            sf.Task.create(data)
            return

    data = {
        'WhoId': LeadId, 
        'Status': 'Open',
        'Description' : messageBody,
        'ActivityDate' : DueDate.isoformat(),
        'Subject': 'Incoming SMS',
        'IsReminderSet': 'true',
        'ReminderDateTime': remainderDate.isoformat()
    }
    print(data)
    sf.Task.create(data)
    
def lambda_handler(event, context):
    # TODO implement
    
    sfdc_username = os.getenv('sfdc_username', 'leadsms@tinosys.com')
    sfdc_pass = os.getenv('sfdc_pass', 'Welcome123$')
    sfdc_token = os.getenv('sfdc_token', 'RpfPxXJlvLAxs7sC03NMStEnv')
    
    if 'Records' not in event.keys():
        event = {'Records': [{'EventSource': 'aws:sns', 'EventVersion': '1.0', 'EventSubscriptionArn': 'arn:aws:sns:us-east-1:146598548507:sms_receive:bddd5760-da3d-4467-ad42-7aec81169ea8', 
        'Sns': {'Type': 'Notification', 'MessageId': '66d36bbb-8617-5a30-a549-327ba121465a', 'TopicArn': 'arn:aws:sns:us-east-1:146598548507:sms_receive', 'Subject': None, 'Message': '{"originationNumber":"+16462069413","destinationNumber":"+18449981182","messageKeyword":"KEYWORD_146598548507","messageBody":"Hello sid ","previousPublishedMessageId":"ddb84611-f6d8-5f80-9e58-1fd1d03f483e","inboundMessageId":"523a2f9c-44e5-4781-9dc9-3e2c2792a2a9"}', 'Timestamp': '2021-09-18T17:42:14.299Z', 'SignatureVersion': '1', 'Signature': 'duiYmdOKA/dJms2OWMF1zeiG77nJBU3tRp8QaecqbqovgA6drI6gDCIw2WpftTEhAL0k8+Q8QVugvt/kjcyP/eNrUdPvXWTHoADeksABw+hmjE36uimkHlFOf2a+d4X3uYAx1E4eR8eynzWGrg33tpl8s5dB8s/K6YWT+ibdc7aUzIJdcXi0GSKcYwrSKI0h5mW0+7hXzF0ACYyyUGJq68VYl6daMWefqzOtmTlF/oC3tMoi1GEKUDgaaH1OV9Rg8rMQIZtbvOmiVFKVmZRZ+aNbhRpcjqB8F55R+ORPKhqq0/exlDiLzJuRJrPMLVg7dk7KMZSe06Er81AEZuI/ng==', 'SigningCertUrl': 'https://sns.us-east-1.amazonaws.com/SimpleNotificationService-7ff5318490ec183fbaddaa2a969abfda.pem', 'UnsubscribeUrl': 'https://sns.us-east-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-east-1:146598548507:sms_receive:bddd5760-da3d-4467-ad42-7aec81169ea8', 'MessageAttributes': {}}}]}

    print(event)

    for rec in event['Records']:
        if 'Sns' in rec.keys():
            for k in rec['Sns'].keys():
                if k == 'MessageId':
                    MessageId = rec['Sns'][k]
                    print(MessageId)
                if k == 'Subject':
                    Subject = rec['Sns'][k]
                    print(Subject)
                if k == 'Message':
                    recvMessage = rec['Sns'][k]
                    if type(recvMessage) != dict:
                        recvMessage = json.loads(recvMessage)
                    print(recvMessage)
                    sendMessageId = recvMessage['previousPublishedMessageId']
                    messageBody = recvMessage['messageBody']
                    print('messageBody', messageBody)
                    print('sendMessageId',sendMessageId)
                    table = boto3.resource('dynamodb').Table('SMSMappingTable')
                    
                    
                    done = False
                    start_key = None
                    while not done:
                    ##while response.get('LastEvaluatedKey', None) is not None:
                        if start_key is None :
                            response = table.scan(FilterExpression=Attr('SendMessageId').eq(sendMessageId))
                        else:
                            response = table.scan(ExclusiveStartKey=start_key, FilterExpression=Attr('SendMessageId').eq(sendMessageId))
                        
                        print(response['ScannedCount'])
                        print(response['Count'])
                        start_key = response.get('LastEvaluatedKey', None)
                        done = start_key is None 

                    for item in response['Items']:
                        ##print('Dynamodb Item', item)
                        ##print(item['LeadId'])
                        try:
                            table.update_item(
                                Key={
                                    'Id': item['Id'],
                                },
                                UpdateExpression= 'SET SMSRecvStatus = :val1, SMSRecvBody = :val2', 
                                ExpressionAttributeValues={
                                    ':val1': 'smsReceived',
                                    ':val2': messageBody
                                }
                            )
                        except botocore.exceptions.ClientError as error:
                            # Put your error handling logic here
                            print('Put Item Error ', error)
                
                        except botocore.exceptions.ParamValidationError as error:
                            print('ParamValidationError ', ValueError('The parameters you provided are incorrect: {}'.format(error)))
                        ##create_taskfor_sms(sfdc_username,sfdc_pass,sfdc_token,item['LeadId'], messageBody,item['ToName'])  
                        #done = True                

                        
    #print(event['Records']['Sns']['Message']['originationNumber'])
    #print(event['Records']['Sns']['Message']['messageBody'])
    
    
    return {
        'statusCode': 200,
        'body': json.dumps('Hello from Lambda!')
    }

